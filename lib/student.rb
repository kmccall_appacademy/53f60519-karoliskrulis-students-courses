class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name, courses = [])
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{self.first_name} #{self.last_name}"
  end

  def enroll(course)
    return if courses.include?(course)
    self.courses << course
    course.students << self
  end

  def has_conflict?(new_course)
    self.courses.any? do |enrolled_course|
      new_course.conflicts_with?(enrolled_course)
    end
  end

  def course_load
    load = Hash.new(0)
    self.courses.each do |c|
      load[c.department] += c.credits
    end
    load
  end



end
